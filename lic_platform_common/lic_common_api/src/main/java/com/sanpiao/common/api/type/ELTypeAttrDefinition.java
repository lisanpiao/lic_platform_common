package com.sanpiao.common.api.type;

import com.baomidou.mybatisplus.annotations.TableField;
import com.baomidou.mybatisplus.annotations.TableName;
import com.sanpiao.common.api.entity.BaseObject;
import lombok.Data;

/**
 * @Description:
 * @Auther: lic
 * @Date: 2018/9/1
 */
@Data
@TableName("sys_lctypeAttrDefinition")
public class ELTypeAttrDefinition extends BaseObject {

    private static final long serialVersionUID = 1L;

    private String name; // 名称
    private String type; // 属性类型

    private String paramName;// 接口参数名称
    private String displayCn;// 中文显示名
    private String displayEn;// 英文显示名
    private String description;// 描述
    @TableField(exist = false)
    private LCTypeDefinition typeDef;//类型定义
    private String typedefId;//类型定义Id
    private String typedefClass;//类型定义名称
    private String logicType;//'0:LOGIC 1:MBA 2:IBA'
    private String fieldColumn;//MBA属性对应的字段名
    private String typeConfig; //属性配置信息
}
