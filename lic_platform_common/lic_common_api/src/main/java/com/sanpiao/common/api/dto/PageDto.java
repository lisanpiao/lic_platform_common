package com.sanpiao.common.api.dto;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.io.Serializable;

/**
 * @Description:
 * @Auther: lic
 * @Date: 2018/9/1
 */
@Data
public class PageDto implements Serializable{

    private static final long serialVersionUID = 1L;

    @ApiModelProperty("页数，默认1")
    private int pageIndex = 1;

    @ApiModelProperty("每页条数，默认10")
    private int pageSize = 10;

    @ApiModelProperty("排序字段，默认create_time")
    private String orderBy = "create_time";

    @ApiModelProperty("排序顺序,倒序")
    private String sortBy = "DESC";
}
