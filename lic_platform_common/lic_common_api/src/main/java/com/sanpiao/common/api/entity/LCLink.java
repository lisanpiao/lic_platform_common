package com.sanpiao.common.api.entity;

import com.baomidou.mybatisplus.annotations.TableField;
import lombok.Data;

import java.lang.reflect.ParameterizedType;
import java.lang.reflect.Type;

/**
 * @Description:
 * @Auther: lic
 * @Date: 2018/9/1
 */
@Data
public class LCLink<A extends BaseObject,B extends BaseObject> extends BaseObject {

    @TableField(value="role_a_id")
    private String roleAId;

    @TableField(value="role_a_classname")
    private String roleAClassName;

    @TableField(value="role_b_id")
    private String roleBId;

    @TableField(value="role_b_classname")
    private String roleBClassName;

    public LCLink(){
        Type genType = getClass().getGenericSuperclass();
        Type[] params = ((ParameterizedType) genType).getActualTypeArguments();
        this.setRoleAClassName(params[0].getTypeName());
        this.setRoleBClassName(params[1].getTypeName());
    }
}
