package com.sanpiao.common.api.dto;

import lombok.Data;

import java.io.Serializable;
import java.util.Date;
import java.util.Map;

/**
 * @Description:
 * @Auther: lic
 * @Date: 2018/9/1
 */
@Data
public class MessageDto implements Serializable{

    private static final long serialVersionUID = 1L;

    /**
     * ID
     */
    private String id;
    /**
     * 项目
     */
    //private String projectName;
    /**
     * 项目ID
     */
    private String projectId;
    /**
     * 目标名字
     */
    private String targetName;
    /**
     * 目标ID
     */
    private String targetId;
    /**
     * 动作目标类型：任务，需求，风险等
     */
    private String targetType;
    /**
     * 创建人
     */
    private Date createTime;
    /**
     * 动作
     */
    private String verb;//动作

    /**
     * 创建人
     */
    private String createBy;
    /**
     * 用户名字
     */
    private String uname;
    /**
     * 用户头像地址
     */
    private String avatar;
    /**
     * 用户实际操作信息
     */
    private Map<String, MessageEntity> data;
}
