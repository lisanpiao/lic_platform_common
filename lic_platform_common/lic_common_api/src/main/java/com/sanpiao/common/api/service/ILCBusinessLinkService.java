package com.sanpiao.common.api.service;

import com.sanpiao.common.api.entity.LCLink;

import java.util.List;

/**
 * @Description:
 * @Auther: lic
 * @Date: 2018/9/1
 */
public interface ILCBusinessLinkService<T extends LCLink> extends ILCService<T> {

    /**
     * 根据A对象ID查询A已经关联的B对象,若bClass传入null，则查询A已经关联的所有业务对象
     * @param aId 对象ID
     * @param bClass 关联的业务对象class
     * @return
     */
    List<T> associatedBObj(String aId, Class<?> bClass);

    /**
     * 根据B对象ID查询B已经关联的A对象
     *
     * @param bId
     *            B对象ID
     * @param bClass
     *            关联的业务对象class
     * @return
     */
    List<T> associatedAObj(String bId, Class<?> bClass);
}
