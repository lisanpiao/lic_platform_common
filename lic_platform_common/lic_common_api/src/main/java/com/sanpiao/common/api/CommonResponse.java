package com.sanpiao.common.api;

import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import com.sanpiao.common.api.constans.CodeUtils;
import com.sanpiao.common.api.dto.LCUserDto;
import lombok.Data;

import java.io.Serializable;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @Description:
 * @Auther: lic
 * @Date: 2018/9/1
 */
@Data
public class CommonResponse implements Serializable {

    private static final long serialVersionUID = 1L;

    private boolean success;
    private String code;
    private String message;
    private Map<String, Object> res;

    /**
     * 默认的构造方法
     */
    public CommonResponse(){
        this.success = true;
    }

    /**
     * 默认失败的构造方法
     * @param statue
     */
    public CommonResponse(boolean statue){
        this.success = statue;
    }

    /**
     * 设置为成功应答
     */
    public void success(){
        this.code = CodeUtils.SUCCESS_CODE;
        this.message = CodeUtils.SUCCESS_MSG;
    }

    /**
     * 向同用应答内设置一项数据对象
     * @param data
     */
    public void setData(Object data){
        if(res == null){
            res = new HashMap<>();
        }
        res.put("data",data);
    }

    /**
     * 向同用应答内设置一项集合对象
     * @param collection
     */
    public void setData(Collection collection){
        if(res == null){
            res = new HashMap<>();
        }
        res.put("data",collection);
    }

    /**
     * 返回人员信息
     * @param collection
     */
    public void setUsers(Collection<LCUserDto> collection){
        if(res == null){
            res = new HashMap<>();
        }
        res.put("users",collection);
    }

    /**
     * 快速创建一个成功应答对象
     * @return
     */
    public static CommonResponse createCommonResponse(){
        CommonResponse commonResponse = new CommonResponse();
        commonResponse.success();
        return commonResponse;
    }

    /**
     * 快速创建一个成功应答，并传入一个数据对象，并设置为success
     * @param date
     * @return
     */
    public static CommonResponse createCommonResponse(Object date){
        CommonResponse commonResponse = new CommonResponse();
        commonResponse.success();
        if(commonResponse.res == null){
            commonResponse.res = new HashMap<>();
        }
        commonResponse.res.put("date",date);
        return commonResponse;
    }

    /**
     * 扩展返回数据对象
     * @param key
     * @param value
     */
    public void extendsRes(String key,Object value ){
        if(res == null){
            res = new HashMap<>();
        }
        res.put(key, value);
    }

    /**
     *  创建一个快速应答，返回一个id
     * @param id
     * @return
     */
    public static CommonResponse createCommonResponseForId(String id){
        CommonResponse commonResponse = new CommonResponse();
        commonResponse.success();
        commonResponse.code = CodeUtils.SUCCESS_CODE;
        commonResponse.setData(id);
        return commonResponse;
    }

    /**
     *  批量修改一个应答对象, 并将修改时间返回回去
     * @param times
     * @return
     */
    public static CommonResponse updateCommonResponseForTimestemp(String times){
        CommonResponse commonResponse = new CommonResponse();
        commonResponse.success();
        commonResponse.code = CodeUtils.SUCCESS_CODE;
        commonResponse.setData(times);
        return commonResponse;
    }

    /**
     *  快速创建一个返回异常信息
     * @param e
     * @return
     */
    public static CommonResponse createExceptionCommonResponse(Exception e){
        CommonResponse commonResponse = new CommonResponse(false);
        commonResponse.setCode(CodeUtils.EXCEPTION_CODE);
        commonResponse.setMessage(CodeUtils.EXCEPTION_MSG + e);
        return commonResponse;
    }

    /**
     * 系统异常
     * @return
     */
    public static CommonResponse createExceptionResponse(){
        CommonResponse commonResponse = new CommonResponse(false);
        commonResponse.setCode(CodeUtils.EXCEPTION_CODE);
        commonResponse.setMessage(CodeUtils.EMPTY_DAT_MSG);
        return commonResponse;
    }

    /**
     *  数据参数不合法
     * @param message
     * @return
     */
    public static CommonResponse createWrongParamCommonResponse(String message){
        CommonResponse commonResponse = new CommonResponse(false);
        commonResponse.setCode(CodeUtils.DATA_WRONGFULNESS_CODE);
        commonResponse.setMessage(CodeUtils.DATA_DUPLICATION_MSG + "-" + message);
        return commonResponse;
    }

    /**
     * 数据集合为空
     * @return
     */
    public static CommonResponse createEmptyListCommonResponse(){
        CommonResponse commonResponse = new CommonResponse(true);
        commonResponse.setCode(CodeUtils.SUCCESS_CODE);
        commonResponse.setMessage(CodeUtils.EMPTY_LIST_MSG);
        commonResponse.setRes(Maps.newHashMap());
        return commonResponse;
    }

    /**
     * 数据为空
     * @return
     */
    public static CommonResponse createEmptyDataCommonResponse(){
        CommonResponse commonResponse = new CommonResponse(true);
        commonResponse.setCode(CodeUtils.SUCCESS_CODE);
        commonResponse.setMessage(CodeUtils.EMPTY_DAT_MSG);
        commonResponse.setRes(Maps.newHashMap());
        return commonResponse;
    }

    /**
     * 数据为null
     * @return
     */
    public static CommonResponse createNotExistCommonResponse(){
        CommonResponse commonResponse = new CommonResponse(false);
        commonResponse.setCode(CodeUtils.DATA_NOT_EXIST_CODE);
        commonResponse.setMessage(CodeUtils.DATA_NOT_EXIST_MSG);
        return commonResponse;
    }

    /**
     * 数据为null
     * @return
     */
    public static CommonResponse createNotExistCommonResponse(String column){
        CommonResponse commonResponse = new CommonResponse(false);
        commonResponse.setCode(CodeUtils.DATA_NOT_EXIST_CODE);
        commonResponse.setMessage(column + " " + CodeUtils.DATA_NOT_EXIST_MSG);
        return commonResponse;
    }

    /**
     * 数据重复
     * @return
     */
    public static CommonResponse createExistCommonResponse(){
        CommonResponse commonResponse = new CommonResponse(false);
        commonResponse.setCode(CodeUtils.DATA_DUPLICATION_CODE);
        commonResponse.setMessage(CodeUtils.DATA_DUPLICATION_MSG);
        return commonResponse;
    }

    /**
     * 自定义返回数据类型
     * @param code
     * @param message
     * @return
     */
    public static CommonResponse createCustomCommonResponse(String code, String message){
        CommonResponse commonResponse = new CommonResponse(false);
        commonResponse.setCode(code);
        commonResponse.setMessage(message);
        return commonResponse;
    }

    public static void main(String[] args) {
        List<String> list = Lists.newArrayList("1","2","4","3");
        CommonResponse commonResponse  = CommonResponse.createCommonResponse(list);
        System.out.println(commonResponse);
    }

}
