package com.sanpiao.common.api.service;

import com.sanpiao.common.api.entity.LCLink;

/**
 * @Description:
 * @Auther: lic
 * @Date: 2018/9/1
 */
public interface ILCLableLinkService<T extends LCLink> extends ILCService<T> {
}
