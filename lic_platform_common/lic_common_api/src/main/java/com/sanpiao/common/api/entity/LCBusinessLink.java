package com.sanpiao.common.api.entity;

import com.baomidou.mybatisplus.annotations.TableField;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.util.Date;

/**
 * @Description:
 * @Auther: lic
 * @Date: 2018/9/1
 */
@Data
public class LCBusinessLink<A extends LCItem,B extends LCItem> extends LCLink<A,B> {

    private static final long serialVersionUID = 7025257821018082644L;

    @ApiModelProperty("创建时间")
    private Date createTime;

    @ApiModelProperty("type, 取值：requirement、task、issue、risk")
    @TableField(exist=false)
    private String type;
}
