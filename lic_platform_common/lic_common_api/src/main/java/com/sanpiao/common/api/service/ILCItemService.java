package com.sanpiao.common.api.service;

import com.baomidou.mybatisplus.mapper.Wrapper;
import com.baomidou.mybatisplus.plugins.Page;
import com.sanpiao.common.api.entity.LCItem;
import com.sanpiao.common.api.type.ELTypeAttrDefinition;

import java.util.List;

/**
 * @Description:
 * @Auther: lic
 * @Date: 2018/9/1
 */
public interface ILCItemService<T extends LCItem> extends ILCService<T>{

    /**
     * 添加
     * @param entity	业务对象实体
     * @param attrDefList		扩展属性列表
     * @return
     */
    boolean insert(T entity, List<ELTypeAttrDefinition> attrDefList);


    /**
     *
     * @param entity	业务对象实体
     * @param attrDefList		扩展属性列表
     * @return
     */
    T insertOne(T entity, List<ELTypeAttrDefinition> attrDefList);


    /**
     *
     * @param entity	业务对象实体
     * @param attrDefList		扩展属性列表
     * @return
     */
    Integer InInsert(T entity, List<ELTypeAttrDefinition> attrDefList);


    /**
     *
     * @param entityList 业务对象实体列表
     * @param attrDefList		扩展属性列表
     * @return
     */
    boolean insertBatch(List<T> entityList, List<ELTypeAttrDefinition> attrDefList);


    /**
     *
     * @param entity	业务对象实体
     * @param attrDefList		扩展属性列表
     * @return
     */
    T update(T entity, List<ELTypeAttrDefinition> attrDefList);


    /**
     *
     * @param entity	业务对象实体
     * @param attrDefList		扩展属性列表
     * @return
     */
    boolean updateById(T entity, List<ELTypeAttrDefinition> attrDefList);


    /**
     *
     * @param entity	业务对象
     * @param wrapper	查询条件实体包装类 {@link Wrapper}
     * @param attrDefList		扩展属性列表
     * @return
     */
    boolean update(T entity, Wrapper<T> wrapper, List<ELTypeAttrDefinition> attrDefList);


    /**
     *
     * @param entityList 业务对象列表
     * @param attrDefList		  扩展属性列表
     * @return
     */
    boolean updateBatchById(List<T> entityList, List<ELTypeAttrDefinition> attrDefList);


    /**
     *
     * @param id		业务对象ID
     * @param attrDefList		扩展属性列表
     * @return
     */
    T queryById(String id, List<ELTypeAttrDefinition> attrDefList);


    /**
     *
     * @param id		业务对象ID
     * @param attrDefList		扩展属性列表
     * @return
     */
    T get(String id, List<ELTypeAttrDefinition> attrDefList);


    /**
     *
     * @param entity	业务对象
     * @param attrDefList		扩展属性列表
     * @return
     */
    T selectOne(Wrapper<T> wrapper, List<ELTypeAttrDefinition> attrDefList);


    /**
     *
     * @param page		分页对象
     * @param attrDefList		扩展属性列表
     * @return
     */
    Page<T> selectPage(Page<T> page, List<ELTypeAttrDefinition> attrDefList);


    /**
     *
     * @param page		分页对象
     * @param wrapper	查询条件实体包装类 {@link Wrapper}
     * @param attrDefList		扩展属性列表
     * @return
     */
    Page<T> selectPage(Page<T> page, Wrapper<T> wrapper, List<ELTypeAttrDefinition> attrDefList);


    /**
     * 查询列表
     * @param wrapper	     查询条件参数
     * @param attrDefList		     扩展属性列表
     * @return
     */
    List<T> selectList(Wrapper<T> wrapper, List<ELTypeAttrDefinition> attrDefList);
}
