package com.sanpiao.common.api.service;

import com.sanpiao.common.api.entity.LCMember;

import java.util.List;

/**
 * @Description:
 * @Auther: lic
 * @Date: 2018/9/1
 */
public interface ILCMemberService<T extends LCMember> extends ILCService<T> {

    /**
     * 更新状态流成员（根据objectId和roleKey删除再添加 members）
     * @param members
     */
    public void updateStateFlowMembers(List<T> members);

    /**
     * 根据RoleKey获取成员ID
     * @param objectId 业务对象ID
     * @param roleKey 角色Key
     * @return
     */
    public List<String> getRoleMembers(String objectId,String roleKey);
}
