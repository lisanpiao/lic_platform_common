package com.sanpiao.common.api.entity;

import com.baomidou.mybatisplus.annotations.TableField;
import com.baomidou.mybatisplus.enums.FieldFill;
import com.sanpiao.common.api.constans.DomainConstans;
import lombok.Data;

import java.util.Date;

/**
 * @Description: Entity实体
 * @Auther: lic
 * @Date: 2018/9/1
 */
@Data
public abstract class LCObject extends BaseObject{

    private static final long serialVersionUID = 1L;

    private String createBy;//创建者

    @TableField(fill = FieldFill.INSERT)
    private Date createTime;//创建时间

    private String updateBy;//更新者

    @TableField(fill = FieldFill.INSERT_UPDATE)
    private Date updateTime;//更新时间

    @TableField(fill = FieldFill.INSERT_UPDATE)
    private String delFlag;//删除标记：（0：正常，1：删除，2：审核）

    public LCObject(){
        super();
        this.delFlag = DomainConstans.DEL_FLAG_NORMAL;
    }
}
