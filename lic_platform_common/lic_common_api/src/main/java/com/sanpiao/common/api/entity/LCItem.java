package com.sanpiao.common.api.entity;

import com.baomidou.mybatisplus.annotations.TableField;
import com.google.common.collect.Maps;

import java.util.Map;

/**
 * @Description:
 * @Auther: lic
 * @Date: 2018/9/1
 */
public class LCItem extends LCObject {

    private static final long serialVersionUID = 1L;

    /**
     * 编码
     */
    private String code;

    /**
     * 名称
     */

    private String name;

    /**
     * 描述
     */
    private String description;


    @TableField(exist = false)
    //private Map<String, Object> extAttr;
    private Map<String, Object> flexAttrs = Maps.newHashMap();

    public LCItem(){
        super();
    }
}
