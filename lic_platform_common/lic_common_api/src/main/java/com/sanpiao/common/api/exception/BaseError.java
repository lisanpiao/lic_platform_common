package com.sanpiao.common.api.exception;

import java.io.Serializable;

/**
 * @Description:
 * @Auther: lic
 * @Date: 2018/9/1
 */
public interface BaseError extends Serializable{

    public String toErrorMessage(String codeMessage);
}
