package com.sanpiao.common.api.exception;

/**
 * @Description:
 * @Auther: lic
 * @Date: 2018/9/1
 */
public class AccessDataException extends Exception{

    private static final long serialVersionUID = 1L;

    public AccessDataException(String message) {
        super(message);
    }
}
