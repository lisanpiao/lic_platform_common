package com.sanpiao.common.api.entity;

import com.baomidou.mybatisplus.annotations.TableId;
import com.baomidou.mybatisplus.enums.IdType;
import lombok.Data;

import java.io.Serializable;

/**
 * @Description: baseEntity
 * @Auther: lic
 * @Date: 2018/9/1
 */
@Data
public abstract class BaseObject implements Serializable {

    private static final long serialVersionUID = 1L;

    @TableId(value = "id",type = IdType.INPUT)
    private String id;
}
