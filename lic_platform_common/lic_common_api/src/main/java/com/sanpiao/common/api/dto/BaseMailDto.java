package com.sanpiao.common.api.dto;

import lombok.Data;

import java.io.Serializable;

/**
 * @Description:
 * @Auther: lic
 * @Date: 2018/9/1
 */
@Data
public class BaseMailDto implements Serializable {

    private static final long serialVersionUID = 1L;
    private String subject; // 邮件主题
    private String from; // 发件人
    private String[] to; // 收件人
    private String[] cc; // 抄送
    private String type = "Text"; // 邮件类型：Text(简单文本)、Html(简单html)、Html_Inline(嵌入静态资源)、Attachment(带附件)
}
