package com.sanpiao.common.api.exception;

/**
 * @Description:
 * @Auther: lic
 * @Date: 2018/9/1
 */
public class SystemException extends RuntimeException {

    public SystemException(){
        super();
    }

    public SystemException(String message){
        super(message);
    }

    public SystemException(Throwable throwable){
        super(throwable);
    }

    public SystemException(String message,Throwable throwable){
        super(message,throwable);
    }
}
