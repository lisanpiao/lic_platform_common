package com.sanpiao.common.api.entity;

import com.baomidou.mybatisplus.annotations.TableField;
import io.swagger.annotations.ApiModelProperty;

import java.lang.reflect.ParameterizedType;
import java.lang.reflect.Type;

/**
 * @Description:
 * @Auther: lic
 * @Date: 2018/9/1
 */
public abstract class LCMember<A extends BaseObject> extends BaseObject{

    private static final long serialVersionUID = 1L;

    @ApiModelProperty("对象ClassName")
    @TableField(value="object_classname")
    private String objectClassName;

    @ApiModelProperty("对象Id")
    private String objectId;

    @ApiModelProperty("流程角色")
    private String roleKey;

    @ApiModelProperty("成员用户Id")
    private String userId;

    @ApiModelProperty("排序")
    private Integer sort;

    @ApiModelProperty("备注")
    private String remark;

    @ApiModelProperty("状态流中当前处理人标识(初始化：0 : 待处理：1 : 已处理：2)")
    private String active;

    @ApiModelProperty("成员头像 ")
    @TableField(exist=false)
    private String avatar;

    @ApiModelProperty("成员用户姓名")
    @TableField(exist=false)
    private String userName;

    public LCMember(Class<BaseObject> clazz){
        this.objectClassName = clazz.getName();
    }
    public LCMember(){
        Type type = getClass().getGenericSuperclass();
        Type[] params = ((ParameterizedType) type).getActualTypeArguments();
        this.objectClassName = params[0].getTypeName();
    }
}
