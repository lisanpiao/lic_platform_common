package com.sanpiao.common.api.dto;

import lombok.Data;

import java.io.Serializable;

/**
 * @Description:
 * @Auther: lic
 * @Date: 2018/9/1
 */
@Data
public class MessageEntity implements Serializable {

    private static final long serialVersionUID = 1L;

    private String eid;
    private String etype;
    private String name;
}
