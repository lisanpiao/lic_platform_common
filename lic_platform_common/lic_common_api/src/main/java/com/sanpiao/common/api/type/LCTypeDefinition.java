package com.sanpiao.common.api.type;

import com.baomidou.mybatisplus.annotations.TableField;
import com.baomidou.mybatisplus.annotations.TableName;
import com.sanpiao.common.api.entity.LCItem;
import lombok.Data;

/**
 * @Description:
 * @Auther: lic
 * @Date: 2018/9/1
 */
@Data
@TableName("sys_lctypedefinition")
public class LCTypeDefinition extends LCItem {

    private static final long serialVersionUID = 1L;
    private String name;
    private String paramName;// 接口参数名称
    private String displayCn;// 中文显示名
    private String displayEn;// 英文显示名
    @TableField(exist = false)
    private LCTypeDefinition parent;// 父类
    private String parentId;// 父类编号
    private String parentClassName;// 父类名称
    private String description;// 描述
    private String icon;// 类型图标
    private Integer instantiable;// 是否可实例化标识，用于后续动态生成类型表
}
