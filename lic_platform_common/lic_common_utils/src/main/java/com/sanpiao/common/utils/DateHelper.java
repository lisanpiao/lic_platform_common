package com.sanpiao.common.utils;

import org.apache.commons.lang3.time.DateFormatUtils;
import org.apache.commons.lang3.time.DateUtils;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.TimeZone;

/**
 * @Description: 日期工具类, 继承org.apache.commons.lang.time.DateUtils类
 * @Auther: lic
 * @Date: 2018/8/19
 */
public class DateHelper extends DateUtils{

    /**
     * 日期格式
     */
    private static final String[] PARSE_PATTERNS = {
            "yyyy-MM-dd", "yyyy-MM-dd HH:mm:ss", "yyyy-MM-dd HH:mm", "yyyy-MM",
            "yyyy/MM/dd", "yyyy/MM/dd HH:mm:ss", "yyyy/MM/dd HH:mm", "yyyy/MM",
            "yyyy.MM.dd", "yyyy.MM.dd HH:mm:ss", "yyyy.MM.dd HH:mm", "yyyy.MM"
    };

    /**
     * 日期格式
     */
    private static final String DEFAULT_DATE_FORMAT = "yyyy-MM-dd";
    /**
     * 日期时间格式
     */
    private static final String DEFAULT_DATETIME_FORMAT = "yyyy-MM-dd HH:mm:ss";

    /**
     * 得到当前日期字符串，格式（yyyy-MM-dd）
     *
     * @return the date
     */
    public static String getDate(){
        return getDate(DEFAULT_DATE_FORMAT);
    }

    /**
     * 得到当前日期字符串 格式（yyyy-MM-dd） pattern可以为："yyyy-MM-dd" "HH:mm:ss" "E"
     *
     * @param pattern the pattern
     * @return the date
     */
    public static String getDate(String pattern) {
        return DateFormatUtils.format(new Date(), pattern);
    }

    /**
     * 得到日期字符串 默认格式（yyyy-MM-dd） pattern可以为："yyyy-MM-dd" "HH:mm:ss" "E"
     * @param date
     * @param pattern
     * @return
     */
    public static String formatDate(Date date,Object... pattern){
        String formatDate;
        if(pattern != null && pattern.length > 0){
            formatDate = DateFormatUtils.format(date,pattern[0].toString());
        }else{
            formatDate = DateFormatUtils.format(date,DEFAULT_DATE_FORMAT);
        }
        return formatDate;
    }

    /**
     * 得到日期时间字符串，转换格式（yyyy-MM-dd HH:mm:ss）
     * @param date
     * @return
     */
    public static String formatDateTime(Date date){
        return formatDate(date,DEFAULT_DATETIME_FORMAT);
    }

    /**
     * 得到当前时间字符串 格式（HH:mm:ss）
     * @return
     */
    public static String getTime(){
        return formatDate(new Date(),"HH:mm:ss");
    }

    /**
     * 得到当前日期和时间字符串 格式（yyyy-MM-dd HH:mm:ss）
     * @return
     */
    public static String getDateTime(){
        return formatDate(new Date(),DEFAULT_DATETIME_FORMAT);
    }

    /**
     * 得到当前年份的字符串 格式（yyyy）
     * @return
     */
    public static String getYear(){
        return formatDate(new Date(),"yyyy");
    }

    /**
     * 得到当前月份的字符串 格式（yyyy）
     * @return
     */
    public static String getMonth(){
        return formatDate(new Date(),"MM");
    }

    /**
     * 得到当天的字符串 格式（yyyy）
     * @return
     */
    public static String getDay(){
        return formatDate(new Date(),"dd");
    }

    /**
     * 得到当前星期的字符串 格式（yyyy）
     * @return
     */
    public static String getWeek(){
        return formatDate(new Date(),"E");
    }

    /**
     * 日期型字符串转化为日期 格式
     * { "yyyy-MM-dd", "yyyy-MM-dd HH:mm:ss", "yyyy-MM-dd HH:mm",
     * "yyyy/MM/dd", "yyyy/MM/dd HH:mm:ss", "yyyy/MM/dd HH:mm",
     * "yyyy.MM.dd", "yyyy.MM.dd HH:mm:ss", "yyyy.MM.dd HH:mm" }
     *
     * @param str
     * @return date
     */
    public static Date parseDate(String str){
        if(str == null){
            return null;
        }
        try{
            return parseDate(str.toString(),PARSE_PATTERNS);
        }catch (ParseException e){
            return null;
        }
    }

    /**
     * 获得过去的时间天数
     * @param date
     * @return
     */
    public static long pastDays(Date date){
        long t = new Date().getTime() - date.getTime();
        return t / (1000 * 24 * 60 * 60);
    }

    /**
     * 获得过去的时间小时数
     * @param date
     * @return
     */
    public static long pastHour(Date date){
        long t = new Date().getTime() - date.getTime();
        return t / (1000 * 60 * 60);
    }

    /**
     * 获得过去的时间分钟数
     * @param date
     * @return
     */
    public static long pastMunites(Date date){
        long t = new Date().getTime() - date.getTime();
        return t / (1000 * 60 * 60);
    }

    /**
     * 转换为时间（天,时:分:秒.毫秒）
     *
     * @param timeMillis 毫秒数
     * @return 天, 时:分:秒.毫秒
     */
    public static String formatDateTime(long timeMillis) {
        long day = timeMillis / (24 * 60 * 60 * 1000);
        long hour = timeMillis / (60 * 60 * 1000) - day * 24;
        long min = timeMillis / (60 * 1000) - day * 24 * 60 - hour * 60;
        long s = timeMillis / 1000 - day * 24 * 60 * 60 - hour * 60 * 60 - min * 60;
        long sss = timeMillis - day * 24 * 60 * 60 * 1000 - hour * 60 * 60 * 1000 - min * 60 * 1000 - s * 1000;
        return (day > 0 ? day + "," : "") + hour + ":" + min + ":" + s + "." + sss;
    }

    /**
     * 获取两个日期之间的天数
     * @param before
     * @param after
     * @return
     */
    public static double getDistanceOfTwoDate(Date before,Date after){
        long beforeTime = before.getTime();
        long afterTime = after.getTime();
        return (afterTime-beforeTime) / (double)(1000 * 60 * 60 * 24);
    }

    /**
     * 获取东八区当前时间
     *
     * @return Date est 8 date
     */
    public static Date getEst8Date() {
        TimeZone tz = TimeZone.getTimeZone("GMT+8:00");
        Calendar calendar = Calendar.getInstance();
        Date date = calendar.getTime();
        SimpleDateFormat dateFormat = new SimpleDateFormat(DEFAULT_DATETIME_FORMAT);
        dateFormat.setTimeZone(tz);
        return parseDate(dateFormat.format(date));
    }

    /**
     * 获得两个日期相隔月份数
     * @param before 较小的日期
     * @param after 较大的日期
     * @return 相差月份数
     */
    public static int getMonthSpace(String before, String after){
        int result = 0;
        Calendar c1 = Calendar.getInstance();
        Calendar c2 = Calendar.getInstance();
        c1.setTime(parseDate(before));
        c2.setTime(parseDate(after));
        result = c1.get(Calendar.MONTH) - c2.get(Calendar.MONTH);
        int month = (c1.get(Calendar.YEAR) - c2.get(Calendar.YEAR)) * 12;
        return Math.abs(month + result);
    }

    /**
     * 获得指定日期最后一天
     * @param date
     * @return
     */
    public static int getLastDateOfMonth(String date) {
        Date d = parseDate(date);
        Calendar a = Calendar.getInstance();
        a.setTime(d);
        a.set(Calendar.DATE, 1);
        a.roll(Calendar.DATE, -1); // 日期回滚一天
        return a.get(Calendar.DATE);
    }

    public static void main(String[] args){
        Date date = new Date();
        String d = formatDateTime(date);
        int day = getLastDateOfMonth(d);
        System.out.print(day);
    }

}
