package com.sanpiao.common.utils;

import org.apache.commons.io.FileUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.*;
import java.nio.file.FileAlreadyExistsException;

/**
 * @Description: 文件操作工具类
 * 文件的创建，删除，复制以及目录的创建、删除、复制等
 * 继承org.apache.commons.io.FileUtils
 * @Auther: lic
 * @Date: 2018/8/19
 */
public class FileHelper extends FileUtils{

    private static final Logger logger = LoggerFactory.getLogger(FileHelper.class);

    /**
     * 文件不存在
     */
    private static final String FILE_NOT_FIND = "%s 文件不存在!";

    /**
     * Buff Size
     */
    private static final int BUF_SIZE = 1024 * 100;

    /**
     * 默认编码
     */
    private static final String DEFAULT_ENCODING = "utf8";

    /**
     * 文件的复制，以及文件夹的复制
     * @param srcFileName
     * @param descFileName
     * @return
     * @throws Exception
     */
    public static boolean copy(String srcFileName,String descFileName) throws IOException{
        File file = new File(srcFileName);
        if(!file.exists()){
            logger.debug(String.format(FILE_NOT_FIND,srcFileName));
            return false;
        }else{
            if(file.isFile()){
                return !copyFile(srcFileName,descFileName);
            }else{
                return !copyDirectory(srcFileName,descFileName);
            }
        }
    }

    /**
     * 复制单个文件，如果目标文件存在，则不覆盖
     * @param srcFileName 源文件
     * @param descFileName 目标文件名
     * @return 如果成功返回true，失败返回false
     * @throws IOException
     */
    public static boolean copyFile(String srcFileName,String descFileName) throws IOException{
        return copyFileCover(srcFileName,descFileName,false);
    }

    /**
     * 复制单个文件，如果目标文件存在，则不覆盖
     * @param srcFileName 源文件
     * @param descFileName 目标文件名
     * @return 如果成功返回true，失败返回false
     * @throws IOException
     */
    public static boolean copyDirectory(String srcFileName,String descFileName) throws IOException{
        return copyDirectoryCover(srcFileName,descFileName,false);
    }

    /**
     * 复制单个文件
     * @param srcFileName
     * @param descFileName
     * @param coverlay 如果文件已经存在，是否覆盖
     * @return 如果成功返回true，失败返回false
     * @throws IOException
     */
    public static boolean copyFileCover(String srcFileName,String descFileName,boolean coverlay) throws  IOException{
        File srcFile = new File(srcFileName);
        //判断源文件是否存在
        if(!srcFile.exists()){
            throw new FileNotFoundException(String.format("复制文件失败，源文件 %s 不存在!",srcFileName));
        }else if(!srcFile.isFile()){
            throw new FileNotFoundException(String.format("复制文件失败，源文件 %s 不存在!",srcFileName));
        }
        File descFile = new File(descFileName);
        //判断目标文件是否存在
        if(descFile.exists()){
            //允许被覆盖
            if(coverlay){
                if(delFile(descFileName)){
                    throw new IOException(String.format("删除目标文件 %s 失败!", descFileName));
                }else{
                    throw new FileAlreadyExistsException(String.format("目标目录复制失败，目标目录 %s 已存在!", descFileName));
                }
            }else{
                if (!descFile.getParentFile().exists() && !mkParentDirs(descFile)) {
                    throw new IOException("创建目标文件所在的目录失败!");
                }
            }
        }
        //复制文件
        try(
                InputStream is = new FileInputStream(srcFile);
                OutputStream os = new FileOutputStream(descFile);
                ){
            copy(is, os);
            return true;
        }catch (Exception e){
            logger.warn("复制文件失败：", e);
            return false;
        }
    }

    /**
     * 复制整个文件内容
     * @param srcFileName
     * @param descFileName
     * @param coverlay 如果文件已经存在，是否覆盖
     * @return 如果成功返回true，失败返回false
     * @throws IOException
     */
    public static boolean copyDirectoryCover(String srcFileName,String descFileName,boolean coverlay) throws  IOException{
        File srcDir = new File(srcFileName);
        // 判断源目录是否存在
        if (!srcDir.exists()) {
            throw new FileNotFoundException(String.format("复制目录失败，源目录 %s 不存在!", srcFileName));
        } else if (!srcDir.isDirectory()) { // 判断源目录是否是目录
            throw new FileNotFoundException(String.format("复制目录失败，%s 不是一个目录!", srcFileName));
        }
        // 如果目标文件夹名不以文件分隔符结尾，自动添加文件分隔符
        String descDirNames = descFileName;
        if (!descDirNames.endsWith(File.separator)) {
            descDirNames = descDirNames + File.separator;
        }
        File descDir = new File(descDirNames);
        // 如果目标文件夹存在
        if (descDir.exists()) {
            if (coverlay) {
                // 允许覆盖目标目录
                if (delFile(descDirNames)) {
                    throw new IOException(String.format("删除目录 %s 失败!", descDirNames));
                }
            } else {
                throw new FileAlreadyExistsException(String.format("目标目录复制失败，目标目录 %s 已存在!", descDirNames));
            }
        } else {
            if (!descDir.mkdirs()) {
                throw new IOException("创建目标目录失败!");
            }
        }

        return copyFolder(srcDir, descFileName);
    }

    /**
     *  复制整个目录的文件
     * @param folder
     * @param descFileName
     * @return
     */
    public static boolean copyFolder(File folder,String descFileName) throws IOException{
        File[] files = folder.listFiles();
        if (files != null) {
            for (File file : files) {
                // 如果是一个单个文件，则直接复制
                if ((file.isFile() && !copyFile(file.getAbsolutePath(), descFileName + file.getName()))
                        || (file.isDirectory() && !copyDirectory(file.getAbsolutePath(), descFileName + file.getName()))) {
                    return false;
                }
            }
        }
        return true;
    }

    /**
     *  删除单个文件或是文件夹
     * @param fileName
     * @return
     */
    public static boolean delFile(String fileName){
        File file = new File(fileName);
        if(!file.exists()){
            logger.debug(FILE_NOT_FIND,fileName);
            return false;
        }else{
            if(file.isFile()){
                return deleteFile(fileName);
            }else{
                return deleteDirectory(fileName);
            }
        }
    }

    /**
     * 删除单个文件
     * @param fileName
     * @return
     */
    public static boolean deleteFile(String fileName){
        File file = new File(fileName);
        if(file.isFile() && file.exists()){
            if(file.delete()){
                logger.debug("删除文件 {} 成功",fileName);
                return true;
            }else{
                logger.debug("删除文件 {} 失败",fileName);
                return false;
            }
        }else{
            logger.debug(FILE_NOT_FIND,fileName);
            return true;
        }
    }

    /**
     *  删除文件夹目录及其文件夹目录以下的文件
     * @param dirName
     * @return
     */
    public static boolean deleteDirectory(String dirName){
        String dirNames = dirName;
        if(!dirNames.endsWith(File.separator)){
            dirNames = dirNames + File.separator;
        }
        File file = new File(dirNames);
        if(!file.exists() || !file.isDirectory()){
            logger.debug("{} 目录不存在",dirNames);
            return true;
        }
        if(clearFolder(file) || file.delete()){
            logger.debug("删除目录 {} 成功!", dirName);
            return true;
        }else{
            logger.debug("删除目录 {} 成功!", dirName);
            return false;
        }
    }

    /**
     * 清空目录
     * @param folder
     * @return
     */
    private static boolean clearFolder(File folder){
        File[] files = folder.listFiles();
        if(files != null){
            for (File file : files) {
                if((file.isFile() && !deleteFile(file.getAbsolutePath()))
                        || (file.isDirectory() && !deleteDirectory(file.getAbsolutePath()))){
                    return false;
                }
            }
        }
        return true;
    }

    /**
     * 创建父目录
     * @param file
     * @return
     */
    private static boolean mkParentDirs(File file){
        return file.getParentFile().mkdirs();
    }

    /**
     * 复制文件
     * @param is
     * @param os
     * @throws IOException
     */
    public static void copy(InputStream is,OutputStream os)throws IOException{
        copy(is,os,BUF_SIZE);
    }

    /**
     * 复制文件
     * @param is
     * @param os
     * @param bufSize
     * @throws IOException
     */
    public static void copy(InputStream is,OutputStream os ,int bufSize) throws IOException{
        byte[] bytes = new byte[bufSize];
        int c;
        try{
            while((c = is.read(bytes)) != -1){
                os.write(bytes,0,c);
            }
        }finally {
           close(is);
        }
    }

    /**
     * 关闭流.
     *
     * @param closeable 流
     */
    public static void close(Closeable closeable) {
        if (closeable != null) {
            try {
                closeable.close();
            } catch (Exception e) {
                logger.error("数据流关闭失败.", e);
            }
        }
    }
}
