package com.sanpiao.common.utils;

/**
 * @Description: 基础异常类
 * @Auther: lic
 * @Date: 2018/8/19
 */
@SuppressWarnings("serial")
public class BaseException extends RuntimeException{

    public BaseException(){
    }

    public BaseException(Throwable throwable){
        super(throwable);
    }

    public BaseException(String message){
        super(message);
    }

    public BaseException(String message,Throwable throwable){
        super(message,throwable);
    }
}
