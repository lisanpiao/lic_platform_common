package com.sanpiao.common.utils;

import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import org.apache.commons.beanutils.BeanMap;
import org.apache.commons.beanutils.BeanUtils;
import org.apache.commons.collections.MapUtils;
import org.apache.poi.ss.formula.functions.T;

import java.util.List;
import java.util.Map;

/**
 * @Description: map集合工具
 * @Auther: lic
 * @Date: 2018/8/19
 */
public class MapUtil extends MapUtils{

    /**
     *  将封装类型转换为map
     * @param t
     * @param <T>
     * @return
     */
    public static <T> Map<String,Object> bean2Map(T t){
        Map<String,Object> map = Maps.newHashMap();
        if(t != null){
            BeanMap beanMap = new BeanMap(t);
            for (Object key : beanMap.keySet()) {
                map.put(String.valueOf(key),beanMap.get(key));
            }
        }
        return map;
    }

    /**
     * 将List<Map<String,Object>>转换为List<T> 
     * @param maps
     * @param clazz
     * @return
     * @throws InstantiationException
     * @throws IllegalAccessException
     */
    public static <T> List<T> maps2Entitys(List<Map<String, Object>> maps, Class<T> clazz) throws InstantiationException, IllegalAccessException{
        List<T> list = Lists.newArrayList();
        if(maps != null && maps.size() > 0){
            Map<String, Object> map = null;
            T et = null;
            for(int i=0; i<maps.size(); i++){
                map = maps.get(i);
                et = map2Entity(map, clazz);
                list.add(et);
            }
        }
        return list;
    }

    /**
     * Map对象转实体对象
     * @param map
     * @param clazz
     * @return
     */
    public static <T> T map2Entity(Map<String,Object> map,Class<T> clazz){
        T t = null;
       try{
            if(map != null){
                t = clazz.newInstance();
                BeanUtils.populate(t,map);
            }
       }catch (Exception e){
            e.printStackTrace();
       }
       return t;
    }
}
