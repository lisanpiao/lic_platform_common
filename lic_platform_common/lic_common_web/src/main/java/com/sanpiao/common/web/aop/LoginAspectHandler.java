package com.sanpiao.common.web.aop;

import com.google.common.base.Strings;
import com.sanpiao.common.api.CommonResponse;
import com.sanpiao.common.api.entity.LCUser;
import com.sanpiao.common.utils.JsonMapper;
import com.sanpiao.common.web.annotation.Logined;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Component;
import org.springframework.web.method.HandlerMethod;
import org.springframework.web.servlet.handler.HandlerInterceptorAdapter;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.lang.reflect.Method;
import java.net.URLDecoder;

/**
 * @Description:
 * @Auther: lic
 * @Date: 2018/9/2
 */
@Component
public class LoginAspectHandler extends HandlerInterceptorAdapter{

    protected Logger logger = LoggerFactory.getLogger(getClass());

    @Override
    public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler) throws Exception{
        HandlerMethod handlerMethod = (HandlerMethod) handler;
        Logined classAnnotation = handlerMethod.getBeanType().getAnnotation(Logined.class);
        Method method = handlerMethod.getMethod();
        Logined methodAnnotation = method.getAnnotation(Logined.class);
        if(classAnnotation != null && methodAnnotation != null){
            String userJson = Strings.isNullOrEmpty(request.getHeader("lc_user_json_str"))
                    ? "" : request.getHeader("lc_user_json_str");
            if(Strings.isNullOrEmpty(userJson)){
                userJson = (String) request.getAttribute("lc_user_json_str");
            }
            if(StringUtils.isBlank(userJson)){
                writeNotLogin(response);
                return false;
            }
            String userInfo = URLDecoder.decode(userJson, "utf-8");
            if(StringUtils.isBlank(userInfo) || StringUtils.isBlank(JsonMapper.fromJsonString(userInfo, LCUser.class).getId())){
                writeNotLogin(response);
                return false;
            }
        }
        return super.preHandle(request, response ,handler);
    }

    private void writeNotLogin(HttpServletResponse response) throws IOException{
        CommonResponse commonResponse = CommonResponse.createCustomCommonResponse(
                HttpStatus.UNAUTHORIZED + "",HttpStatus.UNAUTHORIZED.name());
        response.sendError(HttpStatus.UNAUTHORIZED.value());
        response.getWriter().print(JsonMapper.toJsonString(commonResponse));
        logger.warn("用户没有登录，非法访问");
    }
}
