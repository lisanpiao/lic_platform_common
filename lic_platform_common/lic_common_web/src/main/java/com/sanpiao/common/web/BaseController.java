package com.sanpiao.common.web;

import com.google.common.base.Strings;
import com.sanpiao.common.api.CommonResponse;
import com.sanpiao.common.api.entity.LCUser;
import com.sanpiao.common.api.exception.AccessDataException;
import com.sanpiao.common.utils.JsonMapper;
import com.sanpiao.common.utils.StringHelper;
import com.sanpiao.common.web.editor.DateEditor;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.annotation.Order;
import org.springframework.http.HttpStatus;
import org.springframework.http.converter.HttpMessageNotReadableException;
import org.springframework.web.HttpMediaTypeNotSupportedException;
import org.springframework.web.HttpRequestMethodNotSupportedException;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.MissingServletRequestParameterException;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.*;
import springfox.documentation.spring.web.json.Json;

import javax.servlet.http.HttpServletRequest;
import javax.validation.ConstraintViolation;
import javax.validation.ConstraintViolationException;
import javax.validation.ValidationException;
import java.net.BindException;
import java.net.URLDecoder;
import java.util.Date;
import java.util.Set;

/**
 * 控制器支持类
 *
 * @author ThinkGem
 * @version 2013-3-23
 */
@RestController
@ControllerAdvice
public abstract class BaseController {

    /**
     * 日志对象
     */
    protected Logger logger = LoggerFactory.getLogger(getClass());

    @Autowired
    protected HttpServletRequest request;

    /**
     * 初始化数据绑定 1. 将所有传递进来的String进行HTML编码，防止XSS攻击 2. 将字段中Date类型转换为String类型
     * @param binder
     */
    @InitBinder
    @Order(0)
    public void initBinder(WebDataBinder binder){
        binder.registerCustomEditor(Date.class, new DateEditor());
    }

    /**
     * 用户登录验证 lc_user_json_str
     * @return
     */
    public LCUser getLoginUser(){
        try{
            String userInfo = "";
            if(!Strings.isNullOrEmpty(request.getHeader("lc_user_json_str"))){
                userInfo = URLDecoder.decode(request.getHeader("lc_user_json_str"),"utf-8");
            }
            if(Strings.isNullOrEmpty(userInfo)){
                userInfo = URLDecoder.decode((String) request.getAttribute("lc_user_json_str"),"utf-8");
            }
            return JsonMapper.fromJsonString(userInfo,LCUser.class);
        }catch (Exception e){
            logger.error("获取登录用户异常",e);
            return null;
        }
    }

    /**
     *  参数验证
     * @param exception
     * @return
     * @throws Exception
     */
    @ResponseBody
    @ResponseStatus(code = HttpStatus.BAD_REQUEST)
    @ExceptionHandler(MethodArgumentNotValidException.class)
    public CommonResponse MethodArgumentNotValidHandler(MethodArgumentNotValidException exception) throws Exception{
        logger.error("参数验证失败",exception);
        return CommonResponse.createCustomCommonResponse(String.valueOf(HttpStatus.BAD_REQUEST.value())
                ,"参数验证失败");
    }

    /**
     *  参数绑定验证
     * @param exception
     * @return
     * @throws Exception
     */
    @ResponseBody
    @ResponseStatus(code = HttpStatus.BAD_REQUEST)
    @ExceptionHandler(BindException.class)
    public CommonResponse MethodArgumentNotValidHandler(BindException exception)throws Exception{
        logger.error("参数绑定验证失败");
        return CommonResponse.createCustomCommonResponse(String.valueOf(HttpStatus.BAD_REQUEST.value())
                ,"缺少请求参数");
    }

    /**
     *  缺少请求参数
     * @param exception
     * @return
     */
    @ResponseBody
    @ResponseStatus(code = HttpStatus.BAD_REQUEST)
    @ExceptionHandler(MissingServletRequestParameterException.class)
    public CommonResponse handleMissingServletRequestParameterException(MissingServletRequestParameterException exception){
        logger.error("缺少请求参数");
        return CommonResponse.createCustomCommonResponse(String.valueOf(HttpStatus.BAD_REQUEST.value())
                ,"缺少请求参数");
    }

    /**
     * 参数解析
     * @param exception
     * @return
     */
    @ResponseBody
    @ResponseStatus(code = HttpStatus.BAD_REQUEST)
    @ExceptionHandler(HttpMessageNotReadableException.class)
    public CommonResponse handleHttpMessageNotReadableException(HttpMessageNotReadableException exception){
        logger.error("缺少请求参数");
        return CommonResponse.createCustomCommonResponse(String.valueOf(HttpStatus.BAD_REQUEST.value())
                ,"缺少请求参数");
    }

    /**
     * 参数验证失败
     * @param exception
     * @return
     */
    @ResponseBody
    @ResponseStatus(code = HttpStatus.BAD_REQUEST)
    @ExceptionHandler(ConstraintViolationException.class)
    public CommonResponse handlerServiceException(ConstraintViolationException exception){
        logger.error("参数解析失败",exception);
        Set<ConstraintViolation<?>> constraints = exception.getConstraintViolations();
        ConstraintViolation<?> violation = constraints.iterator().next();
        String message = violation.getMessage();
        return CommonResponse.createCustomCommonResponse(String.valueOf(HttpStatus.BAD_REQUEST.value()),message);
    }

    /**
     * 参数验证失败
     * @param exception
     * @return
     */
    @ResponseBody
    @ResponseStatus(code = HttpStatus.BAD_REQUEST)
    @ExceptionHandler(ValidationException.class)
    public CommonResponse handlerValidationException(ValidationException exception){
        logger.error("参数验证失败");
        return CommonResponse.createCustomCommonResponse(String.valueOf(HttpStatus.BAD_REQUEST.value())
                ,"参数验证失败");
    }

    /**
     * 不支持当前请求
     * @param e
     * @return
     */

    @ResponseBody
    @ResponseStatus(code = HttpStatus.METHOD_NOT_ALLOWED)
    @ExceptionHandler(HttpRequestMethodNotSupportedException.class)
    public CommonResponse handleHttpRequestMethodNotSupportedException(HttpRequestMethodNotSupportedException e) {
        logger.error("不支持当前请求方法", e);
        return CommonResponse.createCustomCommonResponse(String.valueOf(HttpStatus.METHOD_NOT_ALLOWED.value()),
                "不支持当前请求");
    }

    /**
     * 不支持当前媒体类型
     * @param e
     * @return
     */
    @ResponseBody
    @ResponseStatus(code = HttpStatus.UNSUPPORTED_MEDIA_TYPE)
    @ExceptionHandler(HttpMediaTypeNotSupportedException.class)
    public CommonResponse handleHttpMediaTypeNotSupportedException(Exception e) {
        logger.error("不支持当前媒体类型", e);
        return CommonResponse.createCustomCommonResponse(String.valueOf(HttpStatus.UNSUPPORTED_MEDIA_TYPE.value()),
                "不支持当前媒体类型");
    }

    /**
     * 系统异常
     * @param e
     * @return
     */
    @ResponseBody
    @ResponseStatus(code = HttpStatus.INTERNAL_SERVER_ERROR)
    @ExceptionHandler(Exception.class)
    public CommonResponse handleException(Exception e) {
        logger.error("系统异常", e);
        return CommonResponse.createCustomCommonResponse(String.valueOf(HttpStatus.INTERNAL_SERVER_ERROR.value()),
                "系统异常，请稍后重试");
    }

    /**
     * 数据操作权限异常
     * @param e
     * @return
     */
    @ResponseBody
    @ResponseStatus(code = HttpStatus.INTERNAL_SERVER_ERROR)
    @ExceptionHandler(AccessDataException.class)
    public CommonResponse handleAccessDataException(Exception e) {
        logger.error("数据操作权限异常", e);
        return CommonResponse.createCustomCommonResponse(String.valueOf(HttpStatus.OK.value()),
                StringHelper.isBlank(e.getMessage())?"数据操作权限异常":e.getMessage());
    }
}
