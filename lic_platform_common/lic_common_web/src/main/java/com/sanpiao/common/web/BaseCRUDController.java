package com.sanpiao.common.web;

import com.baomidou.mybatisplus.mapper.EntityWrapper;
import com.baomidou.mybatisplus.toolkit.IdWorker;
import com.sanpiao.common.api.CommonResponse;
import com.sanpiao.common.api.entity.BaseObject;
import com.sanpiao.common.api.service.ILCService;
import com.sanpiao.common.utils.GenericeClassUtils;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

/**
 * @Description: 数据基础操作控制器
 * @Auther: lic
 * @Date: 2018/9/2
 */
@Api
@RestController
public class BaseCRUDController<T extends BaseObject,S extends ILCService<T>> extends BaseController{

    protected Class<T> entityClass =(Class<T>) GenericeClassUtils.getSuperClassGenricType(this.getClass(),0);

    @Autowired
    protected S service;

    /**
     *  根据id查询实体
     * @param id
     * @return
     */
    @ApiOperation(value = "根据id查询实体", notes = "selectById")
    @ApiImplicitParam(name = "id", value = "实体id", required = true)
    @ResponseBody
    @GetMapping(value = "/{id}")
    public CommonResponse selectById(@PathVariable(value = "id") final String id){
        T entity = service.selectById(id);
        return CommonResponse.createCommonResponse(entity);
    }

    /**
     *  查询所有实体
     * @return
     */
    @ApiOperation(value = "查询所有实体", notes = "findAllEntitys")
    @ResponseBody
    @GetMapping(value = "/list")
    public CommonResponse findAllEntitys(T entity){
        EntityWrapper<T> wrapper = new EntityWrapper<>();
        wrapper.setEntity(entity);
        return CommonResponse.createCommonResponse(service.selectList(wrapper));
    }

    /**
     * 新增实体
     * @param entity
     * @return
     */
    @ApiOperation(value = "添加实体", notes = "add")
    @ApiImplicitParam(name = "entity", value = "实体Json", required = true, dataType = "application/json")
    @ResponseBody
    @PostMapping(value = "")
    public CommonResponse add(@Valid @RequestBody final T entity){
        entity.setId(IdWorker.get32UUID());
        return service.insert(entity) ? CommonResponse.createCommonResponse(entity)
                : CommonResponse.createExceptionResponse();
    }

    /**
     * 更新实体
     * @param id
     * @param entity
     * @return
     */
    @ApiOperation(value = "更新", notes = "update")
    @ApiImplicitParam(name = "entity", value = "实体Json", required = true, dataType = "application/json")
    @ResponseBody
    @PutMapping(value = "/{id}")
    public CommonResponse update(@PathVariable(value = "id") final String id, @RequestBody final T entity){
        entity.setId(id);
        return service.insert(entity) ? CommonResponse.createCommonResponse(entity)
                : CommonResponse.createExceptionResponse();
    }

    /**
     * 根据id删除实体
     * @param id
     * @return
     */
    @ApiOperation(value = "根据id删除实体", notes = "delete")
    @ApiImplicitParam(name = "id", value = "实体id", required = true, dataType = "Integer")
    @ResponseBody
    @DeleteMapping(value = "/{id}")
    public CommonResponse delete(@PathVariable(value = "id") final String id){
        return service.deleteById(id) ? CommonResponse.createCommonResponse()
                : CommonResponse.createExceptionResponse();
    }
}
