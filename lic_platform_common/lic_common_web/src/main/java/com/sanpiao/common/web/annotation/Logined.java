package com.sanpiao.common.web.annotation;

import static java.lang.annotation.RetentionPolicy.RUNTIME;
import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.Target;

/**
 * @Description:
 * @Auther: lic
 * @Date: 2018/9/2
 */
@Target({ElementType.METHOD,ElementType.TYPE})
@Retention(RUNTIME)
public @interface Logined {
}
