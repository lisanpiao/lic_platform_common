package com.sanpiao.lic_common_mail;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class LicCommonMailApplication {

	public static void main(String[] args) {
		SpringApplication.run(LicCommonMailApplication.class, args);
	}
}
