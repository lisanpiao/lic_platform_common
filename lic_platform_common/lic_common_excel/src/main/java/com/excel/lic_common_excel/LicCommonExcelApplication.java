package com.excel.lic_common_excel;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class LicCommonExcelApplication {

	public static void main(String[] args) {
		SpringApplication.run(LicCommonExcelApplication.class, args);
	}
}
