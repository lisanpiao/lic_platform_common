package com.sanpiao.common.service.datesource;

/**
 * @author wanngxz
 * @Date 2017/03/22
 */
public enum DynamicDataSourceGlobal {
    /**
     * 读数据源
     */
    READ,
    /**
     * 写数据源
     */
    WRITE
}