package com.sanpiao.common.service.filter;

import com.baomidou.mybatisplus.mapper.Condition;
import com.baomidou.mybatisplus.mapper.Wrapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.Serializable;
import java.util.Collection;
import java.util.Collections;
import java.util.List;

/**
 * @Description: 条件拼接中的过滤条件查询
 * @Auther: lic
 * @Date: 2018/8/20
 */
public class FilterUtil {

    protected static Logger logger = LoggerFactory.getLogger(FilterUtil.class);

    /**
     * 匹配类型关键字
     */
    public enum matchType{
        IN, EQ, LIKE, LT, GT, ISNULL, LE, GE;
    }

    /**
     * 解析过滤条件列表字符串 字段名称_值类型_值_比较方式_联合方式_关联实体<br>
     * 值类型分为：
     *
     * <pre>
     * S(String.class), I(Integer.class), L(Long.class), B(Boolean.class), C(Collection.class);
     * </pre>
     *
     * 比较方式分为：
     *
     * <pre>
     * IN, EQ, LIKE, LT, GT, LE, GE;
     * </pre>
     *
     * 联合方式分为：
     *
     * <pre>
     * AND,OR;
     * 若是AND可以留空
     * </pre>
     *
     * 联合方式分为： 若关联实体不是集合而是自身属性，则可以留空
     *
     * <br>
     * 例如：name_S_ch_LIKE__<br>
     * 注意：若参数类型为Boolean时，True必须传入True或true，False可以不设置。例：isSet_B__EQ or
     * isSet_B_True_EQ
     *
     * @param filterStr
     *            name_S_ch_LIKE__
     * @return 若传入参数为空则返回null
     */

}
